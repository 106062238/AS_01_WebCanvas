import '../styles/brushstyle.css';
import Component from './component.js';

export default class BrushStyle extends Component {
    static getRootClass() {
        return '.brush-style';
    }

    constructor(root) {
        super(root);

        this.root.addEventListener('click', this.handleDomClick.bind(this));
    }

    handleDomClick(e) {
        this.fire('click');
    }
}