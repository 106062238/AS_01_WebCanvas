import '../styles/colorstrip.css';
import Component from './component.js';

export default class ColorStrip extends Component {
    static getRootClass() {
        return '.color-picker';
    }

    constructor(root) {
        super(root);

        this.root.addEventListener('click', this.handleDomClick.bind(this));
        this.root.addEventListener('mousedown', this.handleDomMouseDown.bind(this));
        this.root.addEventListener('mouseup', this.handleDomMouseUp.bind(this));
        this.root.addEventListener('mousemove', this.handleDomMouseMove.bind(this));
        this.root.addEventListener('mouseout', this.handleDomMouseOut.bind(this));
        this.initialize();
    }

    isDragged() {
        return this.dragged;
    }

    initialize() {
        this.mouse_x = 0;
        this.mouse_y = 0;
        this.dragged = false;
        this.initCanvasContext();
        this.createGradient();
        this.fillGradient();
        this.drawPickBox();
    }

    initCanvasContext() {
        this.context = this.root.getContext('2d');
        this.context.rect(0, 0, this.root.width, this.root.height);
    }

    createGradient() {
        this.gradient = this.context.createLinearGradient(0, 0, 0, this.root.height);
        this.gradient.addColorStop(0, 'rgba(255, 0, 0, 1)');
        this.gradient.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
        this.gradient.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
        this.gradient.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
        this.gradient.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
        this.gradient.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
        this.gradient.addColorStop(1, 'rgba(255, 0, 0, 1)');
    }

    fillGradient() {
        this.context.fillStyle = this.gradient;
        this.context.fill();
    }

    drawPickBox() {
        this.context.strokeStyle = 'rgba(255, 255, 255, 1)';
        this.context.lineWidth = 2;
        this.context.strokeRect(0, this.mouse_y - 2, this.root.width, 4);
    }

    getDataAndRedraw(e) {
        this.mouse_x = e.offsetX;
        this.mouse_y = e.offsetY;
        this.createGradient();
        this.fillGradient();
        let data = this.context.getImageData(this.mouse_x, this.mouse_y, 1, 1).data;
        this.drawPickBox();
        this.fire('click', `rgba(${data[0]}, ${data[1]}, ${data[2]}, 1)`);
    }

    handleDomClick(e) {
        this.getDataAndRedraw(e);
    }

    handleDomMouseDown(e) {
        this.dragged = true;
    }

    handleDomMouseUp(e) {
        this.dragged = false;
    }

    handleDomMouseMove(e) {
        let data = this.context.getImageData(e.offsetX, e.offsetY, 1, 1).data;
        this.root.title = `rgb(${data[0]}, ${data[1]}, ${data[2]})`;
        if (this.isDragged())
            this.getDataAndRedraw(e);
    }

    handleDomMouseOut(e) {
        this.dragged = false;
    }
}