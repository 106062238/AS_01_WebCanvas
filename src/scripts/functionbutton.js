import '../styles/functionbutton.css';
import Component from './component.js';
import Tool from './tool.js';

export default class FunctionButton extends Component {
    static getRootClass() {
        return '.function-button';
    }

    constructor(root) {
        super(root);

        this.tools = [];
        const ins = root.querySelectorAll(Tool.getRootClass());
        for (let t of ins) {
            const tool = new Tool(t);
            tool.on('click', this.handleToolClick.bind(this));
            this.tools.push(tool);
            if (tool.root.id === 'download')
                this.download = tool;
            else if (tool.root.id === 'upload') {
                this.upload = tool;
                this.upload.on('change', this.handleUploadChange.bind(this));
            }
        }
    }

    reset() {
        this.pickedTool = 'none';
        for (let t of this.tools)
            t.root.classList.remove('picked');
        this.upload.input.value = '';
    }

    changePickedTool(firer) {
        for (let t of this.tools)
            if (t.root.id === this.pickedTool) {
                t.root.classList.remove('picked');
                break;
            }
        this.pickedTool = firer.root.id;
        firer.root.classList.add('picked');
    }

    handleToolClick(firer) {
        if (this.pickedTool !== firer.root.id) {
            if (firer.root.id !== 'download' &&
                firer.root.id !== 'upload' &&
                firer.root.id !== 'undo' &&
                firer.root.id !== 'redo' &&
                firer.root.id !== 'reset')
                this.changePickedTool(firer);
            this.fire('click', firer.root.id);
        }
    }

    handleUploadChange(firer, e) {
        this.fire('change', e);
    }
}