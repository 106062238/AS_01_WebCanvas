import '../styles/fontmenu.css';
import Component from './component.js';
import FontFamily from './fontfamily.js';
import FontSize from './fontsize.js';

export default class FontMenu extends Component {
    static getRootClass() {
        return '.font-menu';
    }

    constructor(root) {
        super(root);

        this.fontFamily = new FontFamily(root.querySelector(FontFamily.getRootClass()));
        this.fontSize = new FontSize(root.querySelector(FontSize.getRootClass()));

        this.fontFamily.on('change', this.handleFontFamilyChange.bind(this));
        this.fontSize.on('change', this.handleFontSizeChange.bind(this));
    }

    handleFontFamilyChange(e) {
        this.fire('change');
    }

    handleFontSizeChange(e) {
        this.fire('change');
    }
}