import '../styles/colorboard.css';
import Component from './component.js';
import ColorBlock from './colorblock.js';
import ColorStrip from './colorstrip.js';

export default class ColorBoard extends Component {
    static getRootClass() {
        return '.color-board';
    }

    constructor(root) {
        super(root);

        this.colorBlock = new ColorBlock(root.querySelector('#color-block'));
        this.colorStrip = new ColorStrip(root.querySelector('#color-strip'));

        this.colorBlock.on('click', this.handleColorBlockClick.bind(this));
        this.colorBlock.on('mousemove', this.handleColorBlockMouseMove.bind(this));
        this.colorStrip.on('click', this.handleColorStripClick.bind(this));
        this.colorStrip.on('mousemove', this.handleColorStripMouseMove.bind(this));
    }

    handleColorBlockClick(firer, blockColor) {
        this.fire('click', blockColor);
    }

    handleColorBlockMouseMove(firer, blockColor) {
        this.fire('mousemove', blockColor);
    }

    handleColorStripClick(firer, stripColor) {
        this.colorBlock.redraw(stripColor);
        let data = this.colorBlock.context.getImageData(this.colorBlock.mouse_x, this.colorBlock.mouse_y, 1, 1).data;
        this.fire('click', `rgba(${data[0]}, ${data[1]}, ${data[2]}, 1)`);
    }

    handleColorStripMouseMove(firer, stripColor) {
        this.colorBlock.redraw(stripColor);
        let data = this.colorBlock.context.getImageData(this.colorBlock.mouse_x, this.colorBlock.mouse_y, 1, 1).data;
        this.fire('mousemove', `rgba(${data[0]}, ${data[1]}, ${data[2]}, 1)`);
    }
}