import '../styles/fontfamily.css';
import Component from './component.js';

export default class FontFamily extends Component {
    static getRootClass() {
        return '.font-family';
    }

    constructor(root) {
        super(root);

        this.root.addEventListener('change', this.handleDomChange.bind(this));
    }

    getFontFamily() {
        return this.root.value;
    }

    handleDomChange(e) {
        this.fire('change');
    }
}