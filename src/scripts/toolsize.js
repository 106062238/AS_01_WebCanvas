import '../styles/slider.css';
import Component from './component.js';

export default class ToolSize extends Component {
    static getRootId() {
        return '#tool-size';
    }

    constructor(root) {
        super(root);

        this.root.addEventListener('mouseover', this.handleDomMouseOver.bind(this));
        this.root.addEventListener('change', this.handleDomChange.bind(this));
    }

    handleDomMouseOver(e) {
        this.root.title = `${this.root.id}: ${this.root.value}px`;
    }

    handleDomChange(e) {
        this.fire('change');
    }
}