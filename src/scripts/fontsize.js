import '../styles/fontsize.css';
import Component from './component.js';

export default class FontSize extends Component {
    static getRootClass() {
        return '.font-size';
    }

    constructor(root) {
        super(root);

        this.root.addEventListener('change', this.handleDomChange.bind(this));
    }

    getFontSize() {
        return this.root.value;
    }

    handleDomChange(e) {
        this.fire('change');
    }
}