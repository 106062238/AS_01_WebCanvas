import '../styles/mainboard.css';
import Component from './component.js';

export default class MainBoard extends Component {
    static getRootClass() {
        return '.main-board';
    }

    constructor(root) {
        super(root);

        this.root.addEventListener('click', this.handleDomClick.bind(this));
        this.root.addEventListener('mousedown', this.handleDomMouseDown.bind(this));
        this.root.addEventListener('mouseup', this.handleDomMouseUp.bind(this));
        this.root.addEventListener('mousemove', this.handleDomMouseMove.bind(this));
        this.root.addEventListener('mouseout', this.handleDomMouseOut.bind(this));
        this.initCanvasContext();
        this.reset();
    }

    isDrawing() {
        return this.drawing;
    }

    isAddingText() {
        return this.addingText;
    }

    isControlled() {
        return this.controlled;
    }

    isImageStackEmpty() {
        return this.imageStack.index <= 1;
    }

    initCanvasContext() {
        this.context = this.root.getContext('2d');
        this.context.rect(0, 0, this.root.width, this.root.height);
    }

    reset(type) {
        this.controlled = false;
        this.drawing = false;
        this.addingText = false;
        this.imageStack = [];
        this.imageStack.index = 0;
        this.imageStack.maxSize = 20;
        this.context.clearRect(0, 0, this.root.width, this.root.height);
        this.root.width = this.root.width;
        this.imageStack[this.imageStack.index++] = this.context.getImageData(0, 0, this.root.width, this.root.height);
        this.context.lineCap = type;
        this.context.lineJoin = 'round';
    }

    brush(start_x, start_y, end_x, end_y, color, size, transparency, type) {
        this.context.globalCompositeOperation = 'source-over';
        this.context.strokeStyle = color;
        this.context.lineWidth = size;
        this.context.globalAlpha = transparency;
        this.context.lineCap = type;
        this.context.moveTo(start_x, start_y);
        this.context.lineTo(end_x, end_y);
        this.context.stroke();
    }

    erase(start_x, start_y, end_x, end_y, size) {
        this.context.globalCompositeOperation = 'destination-out';
        this.context.moveTo(start_x, start_y);
        this.context.lineTo(end_x, end_y);
        this.context.strokeStyle = 'rgba(255, 255, 255, 1)';
        this.context.lineWidth = size;
        this.context.stroke();
    }

    addText(x, y, color, size, transparency, fontFamily, string) {
        this.context.globalCompositeOperation = 'source-over';
        this.context.font = `${size}px ${fontFamily}`;
        this.context.textAlign = 'start';
        this.context.textBaseline = 'top';
        this.context.fillStyle = color;
        this.context.globalAlpha = transparency;
        this.context.fillText(string, x, y, 1000);
    }    

    drawCircle(start_x, start_y, end_x, end_y, color, size, transparency) {
        this.context.putImageData(this.imageStack[this.imageStack.index - 1], 0, 0);
        this.context.globalCompositeOperation = 'source-over';
        this.context.fillStyle = color;
        this.context.lineWidth = size;
        this.context.globalAlpha = transparency;
        this.context.lineCap = 'round';
        this.context.beginPath();
        this.context.arc(start_x, start_y, Math.sqrt(Math.pow(end_x - start_x, 2) + Math.pow(end_y - start_y, 2)), 0, 2 * Math.PI);
        this.context.fill();
    }

    drawTriangle(start_x, start_y, end_x, end_y, color, size, transparency) {
        this.context.putImageData(this.imageStack[this.imageStack.index - 1], 0, 0);
        this.context.globalCompositeOperation = 'source-over';
        this.context.fillStyle = color;
        this.context.lineWidth = size;
        this.context.globalAlpha = transparency;
        this.context.lineCap = 'round';
        this.context.beginPath();
        if (this.isControlled()) {
            let height = end_y - start_y;
            end_x = (end_x < 0)? start_x - height / Math.sqrt(3) : start_x + height / Math.sqrt(3);
        }
        this.context.moveTo(start_x, start_y);
        this.context.lineTo(end_x, end_y);
        this.context.lineTo(2 * start_x - end_x, end_y);
        this.context.fill();
    }

    drawRectangle(start_x, start_y, width, height, color, size, transparency) {
        this.context.putImageData(this.imageStack[this.imageStack.index - 1], 0, 0);
        this.context.globalCompositeOperation = 'source-over';
        this.context.fillStyle = color;
        this.context.lineWidth = size;
        this.context.globalAlpha = transparency;
        this.context.lineCap = 'round';
        this.context.beginPath();
        if (this.isControlled()) {
            let min = Math.min(Math.abs(width), Math.abs(height));
            width = (width < 0)? -min : min;
            height = (height < 0)? -min : min;
        }
        this.context.fillRect(start_x, start_y, width, height);
    }

    drawLine(start_x, start_y, end_x, end_y, color, size, transparency) {
        this.context.putImageData(this.imageStack[this.imageStack.index - 1], 0, 0);
        this.context.globalCompositeOperation = 'source-over';
        this.context.strokeStyle = color;
        this.context.lineWidth = size;
        this.context.globalAlpha = transparency;
        this.context.lineCap = 'round';
        this.context.beginPath();
        if (this.isControlled()) {
            let x = end_x - start_x;
            let y = start_y - end_y;
            let coordinates = this.calculateLineCoordinate(x, y);
            end_x = coordinates[0] + start_x;
            end_y = -coordinates[1] + start_y;
        }
        this.context.moveTo(start_x, start_y);
        this.context.lineTo(end_x, end_y);
        this.context.stroke();
    }

    undo() {
        if (!this.isImageStackEmpty())
            this.context.putImageData(this.imageStack[--this.imageStack.index - 1], 0, 0);
    }

    redo() {
        if (this.imageStack.length > this.imageStack.index)
            this.context.putImageData(this.imageStack[this.imageStack.index++], 0, 0);
    }

    upload(img) {
        this.reset();
        let cw = this.root.width;
        let ch = this.root.height;
        let scale = Math.min(cw / img.width, ch / img.height);
        this.context.drawImage(img, 0, 0, img.width * scale, img.height * scale);
        this.imageStack[0] = this.context.getImageData(0, 0, this.root.width, this.root.height);
    }

    maintainImageStack() {
        if (this.imageStack.length > this.imageStack.index) {
            let i = this.imageStack.index;
            let s = this.imageStack.maxSize;
            this.imageStack = this.imageStack.slice(0, this.imageStack.index);
            this.imageStack.index = i;
            this.imageStack.maxSize = s;
            this.imageStack[this.imageStack.index++] = this.context.getImageData(0, 0, this.root.width, this.root.height);
        } else if (this.imageStack.length >= this.imageStack.maxSize) {
            this.imageStack.shift();
            this.imageStack.push(this.context.getImageData(0, 0, this.root.width, this.root.height));
        } else {
            this.imageStack[this.imageStack.index++] = this.context.getImageData(0, 0, this.root.width, this.root.height);
        }
    }

    calculateLineCoordinate(x, y) {
        let radius = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        let cosine = x / radius;
        let radiant = Math.acos(cosine);
        if (y < 0)
            radiant = 2 * Math.PI - radiant;
        let divdrant = Math.floor(radiant / (Math.PI / 8)) % 16;
        let octdrant = Math.floor((divdrant + 1) / 2) % 8;
        let divRadiant = octdrant * Math.PI / 4;
        let rotateRadiant = Math.abs(radiant - divRadiant);
        if (rotateRadiant > 0.125 * Math.PI)
            rotateRadiant = 2 * Math.PI - radiant;
        let computedRadius = radius * Math.cos(rotateRadiant);
        let coordinateX = computedRadius * Math.cos(divRadiant);
        let coordinateY = computedRadius * Math.sin(divRadiant);
        return [coordinateX, coordinateY];
    }

    handleDomClick(e) {
        this.fire('click', e.offsetX, e.offsetY);
    }

    handleDomMouseDown(e) {
        this.context.beginPath();
        this.fire('mousedown', e.offsetX, e.offsetY);
    }

    handleDomMouseUp(e) {
        this.fire('mouseup');
    }

    handleDomMouseMove(e) {
        this.fire('mousemove', e.offsetX, e.offsetY);
    }

    handleDomMouseOut(e) {
        this.fire('mouseout');
    }
}