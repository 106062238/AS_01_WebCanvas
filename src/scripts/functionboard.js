import '../styles/functionboard.css';
import Component from './component.js';
import ColorBoard from './colorboard.js';
import StyleBoard from './styleboard.js'
import FontMenu from './fontmenu';
import FunctionButton from './functionbutton.js';

export default class FunctionBoard extends Component {
    static getRootClass() {
        return '.function-board';
    }

    constructor(root) {
        super(root);

        this.colorBoard = new ColorBoard(root.querySelector(ColorBoard.getRootClass()));
        this.styleBoard = new StyleBoard(root.querySelector(StyleBoard.getRootClass()));
        this.fontMenu = new FontMenu(root.querySelector(FontMenu.getRootClass()));
        this.functionButton = new FunctionButton(root.querySelector(FunctionButton.getRootClass()));

        this.colorBoard.on('click', this.handleColorBoardClick.bind(this));
        this.colorBoard.on('mousemove', this.handleColorBoardMouseMove.bind(this));
        this.styleBoard.on('click', this.handleStyleBoardClick.bind(this));
        this.styleBoard.on('change', this.handleStyleBoardChange.bind(this));
        this.fontMenu.on('change', this.handleFontMenuChange.bind(this));
        this.functionButton.on('click', this.handleFunctionButtonClick.bind(this));
        this.functionButton.on('change', this.handleFunctionButtonChange.bind(this));
        this.reset();
    }

    reset() {
        this.tool = 'none';
        this.oringalTool = 'none';
        let x = this.colorBoard.colorBlock.mouse_x;
        let y = this.colorBoard.colorBlock.mouse_y;
        let data = this.colorBoard.colorBlock.context.getImageData(x, y, 1, 1).data;
        this.color = `rgba(${data[0]}, ${data[1]}, ${data[2]}, 1)`;
        this.toolSize = this.styleBoard.toolSize.root.value;
        this.toolTransparency = this.styleBoard.toolTransparency.root.value / 100;
        this.brushStyle = this.styleBoard.pickedStyle;
        this.fontFamily = this.fontMenu.fontFamily.getFontFamily();
        this.fontSize = this.fontMenu.fontSize.getFontSize();
        
        this.functionButton.reset();
    }

    handleColorBoardClick(firer, color) {
        this.color = color;
    }

    handleColorBoardMouseMove(firer, color) {
        this.color = color;
    }

    handleStyleBoardClick(firer) {
        this.brushStyle = (firer.pickedStyle === 'zip')? 'butt' : firer.pickedStyle;
    }

    handleStyleBoardChange(firer) {
        this.toolSize = firer.toolSize.root.value;
        this.toolTransparency = firer.toolTransparency.root.value / 100;
    }

    handleFontMenuChange(firer) {
        this.fontFamily = firer.fontFamily.getFontFamily();
        this.fontSize = firer.fontSize.getFontSize();
    }

    handleFunctionButtonClick(firer, id) {
        if (this.tool !== 'download' &&
            this.tool !== 'upload' &&
            this.tool !== 'undo' &&
            this.tool !== 'redo' &&
            this.tool !== 'reset')
            this.oringalTool = this.tool;
        this.tool = id;
        this.fire('click', this.oringalTool);
    }

    handleFunctionButtonChange(firer, e) {
        this.fire('change', e);
    }
}