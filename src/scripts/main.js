import '../styles/main.css'
import Component from './component.js';
import mainBoard from './mainboard.js';
import functionBoard from './functionboard.js';

window.onload = function() {
    new Canvas(document.querySelector('body'));
}

class Canvas extends Component {
    static getRootClass() {
        return '.canvas';
    }

    constructor(root) {
        super(root);

        this.contextBoard = root.querySelector('#context-board');
        this.mainBoard = new mainBoard(root.querySelector(mainBoard.getRootClass()));
        this.functionBoard = new functionBoard(root.querySelector(functionBoard.getRootClass()));

        this.root.addEventListener('keydown', this.handleDomKeyDown.bind(this));
        this.root.addEventListener('keyup', this.handleDomKeyUp.bind(this));
        this.root.addEventListener('keypress', this.handleDomKeyPress.bind(this));
        this.mainBoard.on('click', this.handleMainBoardClick.bind(this));
        this.mainBoard.on('mousedown', this.handleMainBoardMouseDown.bind(this));
        this.mainBoard.on('mouseup', this.handleMainBoardMouseUp.bind(this));
        this.mainBoard.on('mousemove', this.handleMainBoardMouseMove.bind(this));
        this.mainBoard.on('mouseout', this.handleMainBoardMouseOut.bind(this));
        this.functionBoard.on('click', this.handleFunctionBoardClick.bind(this));
        this.functionBoard.on('change', this.handleFunctionBoardChange.bind(this));
        this.reset();
    }

    reset() {
        if (this.mainBoard.isAddingText())
            this.contextBoard.removeChild(this.contextBoard.childNodes[0]);
        this.mainBoard.reset(this.functionBoard.brushStyle);
        this.functionBoard.reset();
    }

    handleDomKeyDown(e) {
        if (this.mainBoard.isAddingText()) {
            let key = e.which || e.keyCode;
            if (key === 8) {
                this.tempDiv.innerHTML = this.tempDiv.innerHTML.substring(0, this.tempDiv.len - 1);
                if (this.tempDiv.len > 0)
                    this.tempDiv.len--;
            }
        } else if (this.mainBoard.isDrawing() && (this.functionBoard.tool === 'triangle' || this.functionBoard.tool === 'rectangle' || this.functionBoard.tool === 'line')) {
            let key = e.which || e.keyCode;
            if (key === 17 && !this.mainBoard.isControlled())
                this.mainBoard.controlled = true;
        }
    }

    handleDomKeyUp(e) {
        if (this.mainBoard.isDrawing() && (this.functionBoard.tool === 'triangle' || this.functionBoard.tool === 'rectangle') || this.functionBoard.tool === 'line') {
            let key = e.which || e.keyCode;
            if (key === 17 && this.mainBoard.isControlled())
                this.mainBoard.controlled = false;
        }
    }

    handleDomKeyPress(e) {
        if (this.mainBoard.isAddingText()) {
            let char = String.fromCharCode(e.which || e.keyCode);
            this.tempDiv.innerHTML += char;
            this.tempDiv.len++;
        }
    }

    handleMainBoardMouseDown(firer, x, y) {
        if (!firer.isDrawing()) {
            if (this.functionBoard.tool !== 'none' && this.functionBoard.tool !== 'text') {
                firer.drawing = true;
                this.start_x = x;
                this.start_y = y;
            }
        }
    }

    handleMainBoardClick(firer, x, y) {
        if (firer.isAddingText()) {
            firer.addingText = false;
            if (this.tempDiv.innerHTML !== '') {
                firer.addText(firer.addTextX,
                firer.addTextY,
                this.functionBoard.color,
                this.functionBoard.fontSize,
                this.functionBoard.toolTransparency,
                this.functionBoard.fontFamily,
                this.tempDiv.innerHTML.substring(0, 25));
                firer.maintainImageStack();
            }
            this.contextBoard.removeChild(this.contextBoard.childNodes[0]);
        } else {
            if (this.functionBoard.tool === 'text') {
                firer.addingText = true;
                firer.addTextX = x;
                firer.addTextY = y;
                let div = document.createElement('div');
                div.style.position = 'absolute';
                div.style.left = `${x}px`;
                div.style.top = `${y}px`;
                div.style.fontFamily = this.functionBoard.fontFamily;
                div.style.fontSize = `${this.functionBoard.fontSize}px`;
                div.style.color = this.functionBoard.color;
                div.style.width = `${Math.min(40 * this.functionBoard.fontSize, 800)}px`;
                div.style.height = `${Math.min(2 * this.functionBoard.fontSize, 200)}px`;
                div.style.border = '1px dashed black';
                div.style.zIndex = '1';
                div.style.wordWrap = 'break-word';
                div.style.lineHeight = '0.9';
                this.contextBoard.appendChild(div);
                this.tempDiv = div;
                this.tempDiv.len = 0;
            }
        }
    }

    handleMainBoardMouseUp(firer) {
        if (firer.isDrawing()) {
            firer.drawing = false;
            firer.controlled = false;
            firer.maintainImageStack();
        }
    }

    handleMainBoardMouseMove(firer, x, y) {
        if (firer.isDrawing()) {
            this.end_x = x;
            this.end_y = y;
            switch (this.functionBoard.tool) {
                case 'brush':
                    firer.brush(this.start_x, this.start_y, this.end_x, this.end_y,
                                this.functionBoard.color,
                                this.functionBoard.toolSize,
                                this.functionBoard.toolTransparency,
                                this.functionBoard.brushStyle);
                    this.start_x = this.end_x;
                    this.start_y = this.end_y;
                    break;
                case 'eraser':
                    firer.erase(this.start_x, this.start_y, this.end_x, this.end_y, this.functionBoard.toolSize);
                    this.start_x = this.end_x;
                    this.start_y = this.end_y;
                    break;
                case 'circle':
                    firer.drawCircle(this.start_x, this.start_y, this.end_x, this.end_y,
                                     this.functionBoard.color,
                                     this.functionBoard.toolSize,
                                     this.functionBoard.toolTransparency);
                    break;
                case 'triangle':
                    firer.drawTriangle(this.start_x, this.start_y, this.end_x, this.end_y,
                                       this.functionBoard.color,
                                       this.functionBoard.toolSize,
                                       this.functionBoard.toolTransparency);
                    break;
                case 'rectangle':
                    firer.drawRectangle(this.start_x, this.start_y, this.end_x - this.start_x, this.end_y - this.start_y,
                                        this.functionBoard.color,
                                        this.functionBoard.toolSize,
                                        this.functionBoard.toolTransparency);
                    break;
                case 'line':
                    firer.drawLine(this.start_x, this.start_y, this.end_x, this.end_y,
                                   this.functionBoard.color,
                                   this.functionBoard.toolSize,
                                   this.functionBoard.toolTransparency);
                    break;
            }
        }
    }

    handleMainBoardMouseOut(firer) {
        if (firer.isDrawing()) {
            firer.drawing = false;
            firer.controlled = false;
            firer.maintainImageStack();
        }
    }

    handleFunctionBoardClick(firer, originalId) {
        switch (firer.tool) {
            case 'download':
                firer.tool = originalId;
                let confirmation = confirm('Sure to download?');
                if (confirmation === true)
                    firer.functionButton.download.root.href = this.mainBoard.root.toDataURL();
                else
                    event.preventDefault();
                break;
            case 'upload':
                firer.tool = originalId;
                break;
            case 'undo':
                firer.tool = originalId;
                this.mainBoard.undo();
                break;
            case 'redo':
                firer.tool = originalId;
                this.mainBoard.redo();
                break;
            case 'reset':
                this.mainBoard.root.classList.remove(originalId);
                this.reset();
                break;
            default:
                this.mainBoard.root.classList.remove(originalId);
                this.mainBoard.root.classList.add(firer.tool);
                break;
        }
    }

    handleFunctionBoardChange(firer, e) {
        let fr = new FileReader();
        let mb = this.mainBoard;
        fr.addEventListener('load', function(e) {
            let img = new Image();
            img.addEventListener('load', function() {
                mb.upload(img);
            });
            img.src = e.target.result;
        });
        fr.readAsDataURL(e.target.files[0]);
    }
}