import '../styles/styleboard.css'
import Component from './component.js';
import ToolSize from './toolsize.js';
import ToolTransparency from './tooltransparency.js';
import BrushStyle from './brushstyle.js';

export default class StyleBoard extends Component {
    static getRootClass() {
        return '.style-board';
    }

    constructor(root) {
        super(root);

        this.brushStyles = [];
        this.pickedStyle = 'round';
        this.toolSize = new ToolSize(root.querySelector(ToolSize.getRootId()));
        this.toolTransparency = new ToolTransparency(root.querySelector(ToolTransparency.getRootId()));
        const ins = root.querySelectorAll(BrushStyle.getRootClass());
        for (let b of ins) {
            const brushStyle = new BrushStyle(b);
            brushStyle.on('click', this.handleBrushStyleClick.bind(this));
            this.brushStyles.push(brushStyle);
        }

        this.toolSize.on('change', this.handleToolSizeChange.bind(this));
        this.toolTransparency.on('change', this.handleToolTransparencyChange.bind(this));
    }

    handleToolSizeChange(firer) {
        this.fire('change');
    }

    handleToolTransparencyChange(firer) {
        this.fire('change');
    }

    handleBrushStyleClick(firer) {
        for (let b of this.brushStyles)
            if (b.root.id === this.pickedStyle) {
                b.root.classList.remove('picked');
                break;
            }
        firer.root.classList.add('picked');
        this.pickedStyle = firer.root.id;
        this.fire('click');
    }
}