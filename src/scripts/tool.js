import '../styles/tool.css';
import Component from './component.js';

export default class Tool extends Component {
    static getRootClass() {
        return '.tool';
    }

    constructor(root) {
        super(root);

        if (root.id === 'upload') {
            this.input = this.root.querySelector('#input');
            this.input.addEventListener('change', this.handleDomChange.bind(this));
        }
        this.root.addEventListener('click', this.handleDomClick.bind(this));
    }

    handleDomClick(e) {
        this.fire('click');
    }

    handleDomChange(e) {
        this.fire('change', e);
    }
}