import '../styles/colorblock.css';
import Component from './component.js';

export default class ColorBlock extends Component {
    static getRootClass() {
        return '.color-picker';
    }

    constructor(root) {
        super(root);
    
        this.root.addEventListener('click', this.handleDomClick.bind(this));
        this.root.addEventListener('mousedown', this.handleDomMouseDown.bind(this));
        this.root.addEventListener('mouseup', this.handleDomMouseUp.bind(this));
        this.root.addEventListener('mousemove', this.handleDomMouseMove.bind(this));
        this.root.addEventListener('mouseout', this.handleDomMouseOut.bind(this));
        this.initialize();
    }

    isDragged() {
        return this.dragged;
    }

    initialize() {
        this.mouse_x = 0;
        this.mouse_y = this.root.height;
        this.dragged = false;
        this.initCanvasContext();
        this.color = 'rgba(255, 0, 0, 255)';
        this.applyRGB(this.color);
        this.drawPickCircle();
    }

    initCanvasContext() {
        this.context = this.root.getContext('2d');
        this.context.rect(0, 0, this.root.width, this.root.height);
    }

    applyRGB(rgbaColor) {
        this.color = rgbaColor;
        this.context.fillStyle = rgbaColor;
        this.context.fill();
        this.applyGradient();
    }

    applyGradient() {
        this.gradient = this.context.createLinearGradient(0, 0, this.root.width, 0);
        this.gradient.addColorStop(0, 'rgba(255, 255, 255, 1)');
        this.gradient.addColorStop(1, 'rgba(255, 255, 255, 0)');
        this.context.fillStyle = this.gradient;
        this.context.fill();
        this.gradient = this.context.createLinearGradient(0, 0, 0, this.root.height);
        this.gradient.addColorStop(0, 'rgba(0, 0, 0, 0)');
        this.gradient.addColorStop(1, 'rgba(0, 0, 0, 1)');
        this.context.fillStyle = this.gradient;
        this.context.fill();
    }

    drawPickCircle() {
        this.context.strokeStyle = 'rgba(255, 255, 255, 1)';
        this.context.lineWidth = 2;
        this.context.beginPath();
        this.context.arc(this.mouse_x, this.mouse_y, 5, 0 * Math.PI, 2 * Math.PI);
        this.context.stroke();
    }

    redraw(rgbaColor) {
        this.root.width = this.root.width;
        this.initCanvasContext();
        this.applyRGB(rgbaColor);
        this.drawPickCircle();
    }

    getDataAndRedraw(e) {
        this.mouse_x = e.offsetX;
        this.mouse_y = e.offsetY;
        this.root.width = this.root.width;
        this.initCanvasContext();
        this.applyRGB(this.color);
        let data = this.context.getImageData(this.mouse_x, this.mouse_y, 1, 1).data;
        this.drawPickCircle();
        this.fire('click', `rgba(${data[0]}, ${data[1]}, ${data[2]}, 1)`);
    }

    handleDomClick(e) {
        this.getDataAndRedraw(e);
    }

    handleDomMouseDown(e) {
        this.dragged = true;
    }

    handleDomMouseUp(e) {
        this.dragged = false;
    }

    handleDomMouseMove(e) {
        let data = this.context.getImageData(e.offsetX, e.offsetY, 1, 1).data;
        this.root.title = `rgb(${data[0]}, ${data[1]}, ${data[2]})`;
        if (this.isDragged())
            this.getDataAndRedraw(e);
    }

    handleDomMouseOut(e) {
        this.dragged = false;
    }
}