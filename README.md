# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
* ```所有物件除了字體和字體大小，當鼠標放上去時都會顯示物件的名稱或值。```
* ```鼠標會依據所選擇的工具在畫布上有不同的圖示，每種圖示的鼠標熱點會有差異。```
* ```字體以外的所有工具在滑鼠離開畫布之後將不具效果。```
## 功能由上到下分別為
### 1. 樣式調整
* **客製調色盤加調色帶**
    * 位置： 右上角。
    * 操作方法： 滑鼠點選或在上面拖曳。
    * 調色帶調整調色盤的色相，工具(除了橡皮擦外)會依據調色盤上選的顏色著色。
* **工具大小與透明度**
    * 位置： 調色區塊下方。
    * 操作方法： 調整滑條。
    * 筆刷、橡皮擦與直線會依據工具大小調整作用大小，工具(除了橡皮擦外)會依據透明度條上的值在著色時會有透明度的變化。
* **筆刷樣式**
    * 位置： 工具樣式下方。
    * 操作方法： 點選。
    * 根據所選的筆刷樣式會有不同，筆刷大小越粗越明顯。
* **字體與字體大小**
    * 位置： 筆刷樣式下方。
    * 操作方法： 點擇。
    * 根據所選的字體與字體大小，畫上字體時會有不同。
### 2. 工具列
* **筆刷**
    * 位置： 工具列第一個筆刷圖案。
    * 操作方法： 點選之後在畫布上點擊或拖曳。
    * 可在畫布上著色、畫畫。
* **橡皮擦**
    * 位置： 工具列第二個橡皮擦圖案。
    * 操作方法： 點選之後在畫布上點擊或拖曳。
    * 將畫布上的顏色除去成白色。
* **增加字體**
    * 位置： 工具列第三個'T'圖案。
    * 操作方法：
        * 點選之後在畫布上點擊所要增加字體的位置之後，以鍵盤輸入英文或字符，輸入完畢再在畫布上任意位置點擊。
        * 在輸入'&'、'<'、'>'之後如想透過backspace刪除的話可能會出現預期外的字符，推測是html的編碼問題。
        * 其餘字符使用backspace皆正常。
    * 在畫布上任意位置增加字體，單次字數最大上限為25字。
* **圓圈**
    * 位置： 工具列第四個圓圈圖案。
    * 操作方法： 點選之後在畫布上點擊或拖曳調整半徑後放開，調整中可以預覽調整後的結果。
    * 在畫布上增加圓形。
* **三角形**
    * 位置： 工具列第五個三角形圖案。
    * 操作方法：
        * 點選之後在畫布上點擊或拖曳調整腰長後放開，調整中可以預覽調整後的結果。
        * 鍵盤案住control可鎖定為正三角形。
    * 在畫布增加等腰三角形。
* **長方形**
    * 位置： 工具列第六個長方形圖案。
    * 操作方法：
        * 點選之後在畫布上點擊或拖曳調整長寬後放開，調整中可以預覽調整後的結果。
        * 鍵盤案住control可鎖定為正方形。
    * 在畫布增加長方形。
* **直線**
    * 位置： 工具列第七個斜線圖案。
    * 操作方法：
        * 點選之後在畫布上點擊或拖曳調整長度方向後放開，調整中可以預覽調整後的結果。
        * 鍵盤案住control可鎖定為45 * k度的相位角直線(k = 0, 1, 2, ..., 7)。
    * 在畫布增加直線。
* **下載**
    * 位置： 工具列第八個下載圖案。
    * 操作方法： 點擊之後在網頁彈出式視窗選擇是否確定下載，確定將執行下載，取消則取消執行。
    * 將目前的畫布轉為圖片下載。
* **上傳**
    * 位置： 工具列第九個上傳圖案。
    * 操作方法： 點擊之後再選擇想要上傳的圖片檔案。
    * 將使用者上傳的圖片置於畫布上，如長寬超出畫布大小，將依據長寬超出的比例自動伸縮。
    * 上傳圖片之後將喪失所有之前的工作紀錄。
    * 可在圖片上作圖。
* **上一步**
    * 位置： 工具列第十個左向箭頭圖案。
    * 操作方法： 點擊。
    * 還原上一步的工作紀錄，最舊工作紀錄紀錄至20次以前(最多還原至20次以前)。
    * 沒有工作進度或已經到達最舊工作紀錄點擊無效果。
* **下一步**
    * 位置： 工具列第十一個右向箭頭圖案。
    * 操作方法： 點擊。
    * 執行下一步的工作紀錄，沒有更新的工作紀錄點擊無效果。

```如以工具作圖時畫布上的工作紀錄不是最新的工作紀錄，新的作圖將會以新的工作紀錄路徑，覆蓋掉目前畫布上工作紀錄之後的所有工作紀錄。```

* **重置**
    * 位置： 工具列第十一個右向箭頭圖案。
    * 操作方法： 點擊。
    * 將畫布初始化還原，並初始化所有工作紀錄，但不初始化樣式設定。